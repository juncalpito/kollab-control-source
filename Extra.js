function findStaffPosition(sheet, findElement) {
  
  var range = sheet.getDataRange()
  var values = range.getValues()

  for(var i=3; i<range.getLastColumn(); i++) {
    var columnElement = values[7][i] // Row 8 column elements
    if(columnElement == findElement) {
      var columnPosition = {
        found: true,
        startCol: i+1
      }
    } else if(columnElement == "TOTAL") { // TOTAL column element in W31 tables
      var unassignedPosition = {
        found: false,
        startCol: i+1
      }
    }
  }

  if(typeof columnPosition != "undefined") {
    return columnPosition
  } else {
    return unassignedPosition
  }
}

function findProject(sheet, startRow, endRow, endWord, projectId) {

  var range = sheet.getDataRange()
  var values = range.getValues()

  for(var i=startRow; i<endRow; i++) {
    var row = values[i]
    var rowElement = row[3] // 4th Column 
    
    if(rowElement == projectId) {
      Logger.log("Row element and projectId matched")
      var rowPosition = {
        found: true,
        startRow: i+1
      }
    } else if(rowElement == endWord) {
      var endPosition = {
        found: false,
        startRow: i+1
      }
    }
  }
  
  if(typeof rowPosition != "undefined") {
    return rowPosition
  } else {
    return endPosition
  }

}

function findOOPRow() {

  var sheet = SpreadsheetApp.getActiveSheet()
  
  var range = sheet.getDataRange()
  var values = range.getValues()

  for(var i=0; i<values.length; i++) {
    var row = values[i]
    var rowElement = row[3] // 4th Column 
    
    if(rowElement == 'OOP') {
      var rowPosition = {
        startRow: i+1,
        startCol: 3
      }
    }
  }

  return rowPosition
}

function rowAndColumnTrueControl(startRow, startCol, cellValue) {
  
  var sheet = SpreadsheetApp.getActiveSheet()
  var range = sheet.getDataRange()
  var cell = range.getCell(startRow,startCol)
  if(cell == "") {
    cell.setValue(cellValue)
  } else {
    var amount = cell.getValue()
    amount += cellValue
    cell.setValue(amount)
  }
  
}

function rowFalseColumnTrueControl(startRow, startCol, rowValue, cellValue) {
  
  var sheet = SpreadsheetApp.getActiveSheet()
  var range = sheet.getDataRange()
  sheet.insertRowAfter(startRow)
  var rowContents = [
    '',
    '',
    '',
    rowValue
  ]
  sheet.getRange(startRow+1, 1, 1, rowContents.length).setValues([rowContents])
  range = sheet.getDataRange()
  range.getCell(startRow+1,startCol).setValue(cellValue)
  
}

function rowTrueColumnFalseControl(startRow, startCol, columnValue, cellValue) {
  
  var sheet = SpreadsheetApp.getActiveSheet()
  sheet.insertColumnsBefore(startCol,1)
  var range = sheet.getDataRange()
  range.getCell(8,startCol).setValue(columnValue) // Insert @ Row 8
  range.getCell(startRow,startCol).setValue(cellValue)
  
}

function rowAndColumnFalseControl(startRow, startCol, columnValue, rowValue, cellValue) {
  
  var sheet = SpreadsheetApp.getActiveSheet()
  sheet.insertColumnsBefore(startCol,1)
  var range = sheet.getDataRange()
  range.getCell(8,startCol).setValue(columnValue) // Insert @ Row 8
  sheet.insertRowAfter(startRow)
  var rowContents = [
    '',
    '',
    '',
    rowValue
  ]
  sheet.getRange(startRow+1, 1, 1, rowContents.length).setValues([rowContents])
  range = sheet.getDataRange()
  range.getCell(startRow+1,startCol).setValue(cellValue)
  
}

// ============================================================================
// =================================PART II====================================
// ============================================================================

function findWord(key) {

  var sheet = SpreadsheetApp.getActiveSheet()
  var range = sheet.getDataRange()
  var values = range.getValues()
  
  for(var i=6; i<values.length; i++) {
    var row = values[i]
    var rowElement = row[3]
    if(rowElement == key) {
      return i
    }
  }

}

function total(startRow, rowLength) {

  getSumOfRow(startRow+2, rowLength)
  getSumOfColumn(startRow+2, rowLength)
  
}

function totalGrossActivities() {
  
  var startGross = findWord("CONTROL CHECK (GROSS ACTIVITIES)")
  var lastGross = findWord("TOTAL ACTIVITIES")
  getSumOfRow(startGross+2, lastGross)
  getSumOfColumn(startGross+2, lastGross)
  
}

function getSumOfRow(startRow, rowLength) {
  
  var sheet = SpreadsheetApp.getActiveSheet()
  var range = sheet.getDataRange()
  var values = range.getValues()
  var rowTotals = []
  
  for(var i=startRow; i<rowLength+1; i++) {
    var row = values[i]
    var column = range.getLastColumn()
    var j=5
    var total = 0
    
    while(j<column) {
      var cell = range.getCell(i, j).getValue()
      Logger.log("Row " + i + " column " + j + " " + cell)
      if(cell != "") {
        total += cell
      }
      j++
    }
    rowTotals.push(total)
  }
  
  recordSumOfRow(sheet, rowTotals, startRow, rowLength)
  
}

function testGetSumOfRow() {

  getSumOfRow(W32_STAFF_PROJECT)
  
}

function recordSumOfRow(sheet, rowTotals, startRow, rowLength) {

  var range = sheet.getDataRange()
  var values = range.getValues()
  var numTotal = 0
  for(var i=startRow; i<rowLength+1; i++) {
    var row = values[i]
    var column = range.getLastColumn()
    var cell = range.getCell(i, column).setValue(rowTotals[numTotal])
    numTotal++
  }
}

// (12, projectLength)
function getSumOfColumn(startRow, lastRow) {

  var sheet = SpreadsheetApp.getActiveSheet()
  var range = sheet.getDataRange()
  var values = range.getValues()
  var columnTotals = []
  
  for(var i=5; i<range.getLastColumn()+1; i++) {
    var j=startRow
    var total = 0
    
    while(j<lastRow+1) {
      var cell = range.getCell(j, i).getValue()
      if(cell != "") {
        total += cell
      }
      j++
    }
    columnTotals.push(total)
    Logger.log("Total for column " + i + " " + total)
  }
  recordSumOfColumn(sheet, columnTotals, lastRow)
}

function recordSumOfColumn(sheet, columnTotals, lastRow) {

  var range = sheet.getDataRange()
  var columnLength = range.getLastColumn()+1
  var numTotal = 0
  for(var i=5; i<columnLength; i++) {
    var cell = range.getCell(lastRow+1, i).setValue(columnTotals[numTotal])
    numTotal++
  }
  
}

function getSumofTwoRows(firstRow, secondRow, destinationRow) {

  var sheet = SpreadsheetApp.getActiveSheet()
  var range = sheet.getDataRange()
  var values = range.getValues()
  var columnTotals = []
  
  for(var i=5; i<range.getLastColumn()+1; i++) {
    var firstSum = range.getCell(firstRow, i).getValue()
    var secondSum = range.getCell(secondRow, i).getValue()
    var total = firstSum + secondSum
    Logger.log("Column " + i + " total expenses " + total)
    range.getCell(destinationRow, i).setValue(total)
  }
  
}
