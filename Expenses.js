var sheet = SpreadsheetApp.getActiveSheet()

function getDates() {
  
  var range = sheet.getDataRange()
  var values = range.getValues()
  
  var START = values[0][1]
  var END = values[1][1]
  
  var DATES = {
    START: START,
    END: END
  }
  return DATES
  
}

function getProject() {

  var range = sheet.getDataRange()
  var values = range.getValues()
  
  var projectId = values[2][1]
  projectId = projectId.split(",")
  
  return projectId

}

function getStaff() {

  var range = sheet.getDataRange()
  var values = range.getValues()
  
  var staffId = values[3][1] // 4th Row 2 Column
  staffId = staffId.split(",")

  return staffId
  
}

function getOOPExpenseEntries() {
  
  var dataDumpSpreadsheet = SpreadsheetApp.openById(SHEETS_ID)
  var expenseSheet = dataDumpSpreadsheet.getSheetByName(SHEETS_TAB_EXPENSES_MASTER)
  var entriesRange = expenseSheet.getDataRange()
  var entriesValues = entriesRange.getValues()

  //Remove header
  entriesValues.splice(0,1)
  
  var entriesByDate = filterByDate(entriesValues)
  var entriesByProject = filterByProject(entriesByDate)
  var entriesByStaff = filterByStaff(entriesByProject)
  
  return entriesByStaff
  
}

function filterByDate(oopEntriesValues) {

  var DATES = getDates()
  Logger.log(DATES)

  var entries = []

  // Create an array of rows
  for (var i=0; i<oopEntriesValues.length; i++) {
    var row = oopEntriesValues[i]
    var shareWith = row[6]
    var date = Utilities.formatDate(new Date(row[4]), Session.getScriptTimeZone(), "yyyy-MM-dd")
  
    if(DATES.START != "" && DATES.END != "") {
      
      var START_DATE = Utilities.formatDate(new Date(DATES.START), Session.getScriptTimeZone(), "yyyy-MM-dd")
      var END_DATE = Utilities.formatDate(new Date(DATES.END), Session.getScriptTimeZone(), "yyyy-MM-dd")
      
      if(shareWith == "" && date >= START_DATE && date <= END_DATE) {
        var entry = {
          paymentMethod: row[1],
          payDate: row[4],
          amount: row[5],
          sharedWith: row[6],
          expensetype: row[7],
          numOfDays: row[12],
          projectId: row[13],
          employeeId: row[18],
          adjustment: row[19]
        }
        
        entries.push(entry)
      }
      
    } else {
      if(shareWith == "") {
        var entry = {
          paymentMethod: row[1],
          payDate: row[4],
          amount: row[5],
          sharedWith: row[6],
          expensetype: row[7],
          numOfDays: row[12],
          projectId: row[13],
          employeeId: row[18],
          adjustment: row[19]
        }
        
        entries.push(entry)
      }
    }
  }
  
  return entries
  
}

function filterByProject(oopEntriesValues) {

  var project = getProject()

  var entries

  if (project != "") {
    entries = []
    for (var i=0; i<oopEntriesValues.length; i++) {
      var record = oopEntriesValues[i]
      
      for(var j=0; j<project.length; j++) {
      
        if(project[j].trim() == record.projectId) {
          Logger.log(project[j] + ' and ' + record.projectId + ' match.')
          entries.push(record)
        }
      
      }
    
    }
  } else {
    return oopEntriesValues
    Logger.log("Filtered by project " + entries)
  }
  
  return entries
  
}

function filterByStaff(oopEntriesValues) {

  var staff = getStaff()

  var entries

  if (staff != "") {
    entries = []
    for (var i=0; i<oopEntriesValues.length; i++) {
      var record = oopEntriesValues[i]
      
      for(var j=0; j<staff.length; j++) {
      
        if(staff[j].trim() == record.employeeId) {
          Logger.log(staff[j] + ' and ' + record.employeeId + ' match.')
          entries.push(record)
        }
      
      }
    
    }
  } else {
    return oopEntriesValues
    Logger.log("Filtered by staff " + entries)
  }
  
  return entries
  
}

function testGetOOPCategorizedExpenseEntries() {

  var START = "2017-11-08"
  var END = "2017-11-10"  
  
  getOOPExpenseEntries(START, END)
  
}

function getBeeboleEntries() {
  
  var dataDumpSpreadsheet = SpreadsheetApp.openById(SHEETS_ID)
  var beeboleSheet = dataDumpSpreadsheet.getSheetByName(SHEETS_TAB_BEEBOLE)
  var beeboleRange = beeboleSheet.getDataRange()
  var beeboleValues = beeboleRange.getValues()

  //Remove header
  beeboleValues.splice(0,1)
  
  var entriesByDate = filterHoursByDate(beeboleValues)
  var entriesByProject = filterByProject(entriesByDate)
  var entriesByStaff = filterByStaff(entriesByProject)
  
  // Logger.log("Final Record: " + entriesByStaff)
  
  return entriesByStaff
  
}

function filterHoursByDate(beeboleValues) {

  var DATES = getDates()

  var entries = []

  // Create an array of rows
  for (var i=0; i<beeboleValues.length; i++) {
    var row = beeboleValues[i]
    var isNonWorking = row[12]
    var entryStatus = row[4]
    var date = Utilities.formatDate(new Date(row[6]), Session.getScriptTimeZone(), "yyyy-MM-dd")
  
    if(DATES.START != "" && DATES.END != "") {
      
      var START_DATE = Utilities.formatDate(new Date(DATES.START), Session.getScriptTimeZone(), "yyyy-MM-dd")
      var END_DATE = Utilities.formatDate(new Date(DATES.END), Session.getScriptTimeZone(), "yyyy-MM-dd")
      
      if(entryStatus == "a" && date >= START_DATE && date <= END_DATE) {
        var entry = {
          taskId: row[1],
          hours: row[3],
          date: row[6],
          projectId: row[9],
          employeeId: row[11],
          isNonWorking: row[12]
          // TODO
          // add subproject
          // subprojectId: row[]
        }
        
        entries.push(entry)
      }
      
    } else {
        
      if(entryStatus == "a") {
        var entry = {
          taskId: row[1],
          hours: row[3],
          date: row[6],
          projectId: row[9],
          employeeId: row[11],
          isNonWorking: row[12]
        }
        
        entries.push(entry)
      }
      
    }
  }
  
  return entries
  
}