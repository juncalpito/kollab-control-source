function generateReport() {

  recordBillableHourstoControlSouce()
  recordNonBillableHoursToControlSource()
  recordTotalHours()
  recordBillableExpensetoControlSouce()
  recordNonBillableExpenseToControlSource()
  recordTotalExpenses()
  recordToAmexRow()
  recordToCapOneRow()
  recordToOOPRow()
  recordToAdjustmentRow()
  
}

function recordBillableExpensetoControlSouce() {
  var expense = getOOPExpenseEntries()

  var expByStaff = {};

  // Group record by staff. Get only client chargeable records.
  for(var i in expense) {
    var row = expense[i]
    if(row.expensetype == "CLIENT_CHARGEABLE") {
      if (typeof expByStaff[row.employeeId] !== 'undefined') {
        expByStaff[row.employeeId].push(row)
      }
      else {
        expByStaff[row.employeeId] = new Array()
        expByStaff[row.employeeId].push(row)
      }
    }
  }

  var controlSourceSheet = SpreadsheetApp.getActiveSheet()
  var range = controlSourceSheet.getDataRange()
  var values = range.getValues()

  for(var key in expByStaff) {
    if(expByStaff.hasOwnProperty(key)) {
      for(var i=0; i<expByStaff[key].length; i++) {
        var record = expByStaff[key][i]
        
        // Get Range or number of rows to check project position
        var startRow = findWord('PROJECTS')
        var endRow = findWord('TOTAL BILLABLE EXPENSES')
        
        var projectPosition = findProject(controlSourceSheet, startRow, endRow+1, 'PROJECTS', record.projectId) // Row
        var staffPosition = findStaffPosition(controlSourceSheet, key) // Column

        var range = controlSourceSheet.getDataRange()
        if(staffPosition.found == true && projectPosition.found == true) {
          // (startRow, startCol, cellValue)
          rowAndColumnTrueControl(projectPosition.startRow, staffPosition.startCol, record.amount)

        // Append new row for staff and record amount to column of project
        } else if (staffPosition.found == true && projectPosition.found == false) { 
          // (startCol, rowValue, cellValue)
          rowFalseColumnTrueControl(projectPosition.startRow, staffPosition.startCol, record.projectId, record.amount)
        
        // Add new column before "Unassigned" and set amount at correct cell
        } else if (staffPosition.found == false && projectPosition.found == true) {
          // (startRow, startCol, columnValue, cellValue)
          rowTrueColumnFalseControl(projectPosition.startRow, staffPosition.startCol, record.employeeId, record.amount)

        // Append new Row for staff and insert new column before "Unassigned" and set value to project Id
        } else {
          // (startRow, startCol, columnValue, rowValue, cellValue)
          rowAndColumnFalseControl(projectPosition.startRow, staffPosition.startCol, record.employeeId, record.projectId, record.amount)
        }
      }
    } 
  }
  
  // Gets sum of elements per row starting from startRow
  var startRow = findWord("PROJECTS")
  var endRow = findWord("TOTAL BILLABLE EXPENSES")
  total(startRow, endRow)
}

function recordNonBillableExpenseToControlSource() {

  var expense = getOOPExpenseEntries()

  var expByStaff = {}

  // Group record by staff
  for(var i in expense) {
    var row = expense[i]
    if(row.expensetype !== 'CLIENT_CHARGEABLE') {
      if (typeof expByStaff[row.employeeId] !== 'undefined') {
        expByStaff[row.employeeId].push(row)
      }
      else {
        expByStaff[row.employeeId] = new Array()
        expByStaff[row.employeeId].push(row)
      }
    } 
  }

  var controlSourceSheet = SpreadsheetApp.getActiveSheet()
  var range = controlSourceSheet.getDataRange()
  var values = range.getValues()
  
  // Get row for personal and non_client_team expense type
  var personalExpRow = findWord('PERSONAL EXPENSE')
  var nonClientRow = findWord('NON-CLIENT/TEAM CHARGES')

  for(var key in expByStaff) {
    if(expByStaff.hasOwnProperty(key)) {
      for(var i=0; i<expByStaff[key].length; i++) {
        var record = expByStaff[key][i]
        var staffPosition = findStaffPosition(controlSourceSheet, key) // Column
        var startRow = 0
        if(record.expensetype == 'PERSONAL') {
          startRow = personalExpRow+1
        } else {
          startRow = nonClientRow+1
        }

        var range = controlSourceSheet.getDataRange()
        if(staffPosition.found == true) {
          // (startRow, startCol, cellValue)
          rowAndColumnTrueControl(startRow, staffPosition.startCol, record.amount)

        // Add new column before "Unassigned" and set amount at correct cell
        } else if (staffPosition.found == false) {
          // (startRow, startCol, columnValue, cellValue)
          rowTrueColumnFalseControl(startRow, staffPosition.startCol, record.employeeId, record.amount)
        }
      }
    } 
  }
  
  // Gets sum of elements per row starting from startRow
  var startRow = findWord("TOTAL BILLABLE EXPENSES")+1
  var endRow = findWord("TOTAL NON-BILLABLE EXPENSES")
  total(startRow, endRow)
}

function recordTotalExpenses() {

  var totalBillableExp = findWord("TOTAL BILLABLE EXPENSES")
  var totalNonBillableExp = findWord("TOTAL NON-BILLABLE EXPENSES")
  var totalExp = findWord('TOTAL EXPENSES')
  getSumofTwoRows(totalBillableExp+1, totalNonBillableExp+1, totalExp+1)
  
}

function recordToAmexRow() {
   var expense = getOOPExpenseEntries()

  var expByStaff = {};

  // Group record by staff. Get only AMEX payment method expenses.
  for(var i in expense) {
    var row = expense[i]
    if(row.paymentMethod == "AMEX" && row.adjustment == "") {
      if (typeof expByStaff[row.employeeId] !== 'undefined') {
        expByStaff[row.employeeId].push(row)
      }
      else {
        expByStaff[row.employeeId] = new Array()
        expByStaff[row.employeeId].push(row)
      }
    }
  }

  var controlSourceSheet = SpreadsheetApp.getActiveSheet()
  var range = controlSourceSheet.getDataRange()
  var values = range.getValues()
  
  var amexRow = findWord('AMEX')

  for(var key in expByStaff) {
    if(expByStaff.hasOwnProperty(key)) {
      for(var i=0; i<expByStaff[key].length; i++) {
        var record = expByStaff[key][i]
        var staffPosition = findStaffPosition(controlSourceSheet, key) // Column

        var range = controlSourceSheet.getDataRange()
        if(staffPosition.found == true) {
          // (startRow, startCol, cellValue)
          rowAndColumnTrueControl(amexRow+1, staffPosition.startCol, record.amount)

        // Add new column before "Unassigned" and set amount at correct cell
        } else if (staffPosition.found == false) {
          // (startRow, startCol, columnValue, cellValue)
          rowTrueColumnFalseControl(amexRow+1, staffPosition.startCol, record.employeeId, record.amount)
        }
      }
    } 
  }
}

function recordToCapOneRow() {
   var expense = getOOPExpenseEntries()

  var expByStaff = {};

  // Group record by staff. Get only visa or master payment method expenses.
  for(var i in expense) {
    var row = expense[i]
    if((row.paymentMethod == "VISA" || row.paymentMethod == "MASTER") && row.adjustment == "") {
      if (typeof expByStaff[row.employeeId] !== 'undefined') {
        expByStaff[row.employeeId].push(row)
      }
      else {
        expByStaff[row.employeeId] = new Array()
        expByStaff[row.employeeId].push(row)
      }
    }
  }

  var controlSourceSheet = SpreadsheetApp.getActiveSheet()
  var range = controlSourceSheet.getDataRange()
  var values = range.getValues()
  
  var capRow = findWord('CAP1')

  for(var key in expByStaff) {
    if(expByStaff.hasOwnProperty(key)) {
      for(var i=0; i<expByStaff[key].length; i++) {
        var record = expByStaff[key][i]
        var staffPosition = findStaffPosition(controlSourceSheet, key) // Column
        Logger.log(staffPosition)
        var range = controlSourceSheet.getDataRange()
        if(staffPosition.found == true) {
          // (startRow, startCol, cellValue)
          rowAndColumnTrueControl(capRow+1, staffPosition.startCol, record.amount)

        // Add new column before "Unassigned" and set amount at correct cell
        } else if (staffPosition.found == false) {
          // (startRow, startCol, columnValue, cellValue)
          rowTrueColumnFalseControl(capRow+1, staffPosition.startCol, record.employeeId, record.amount)
        }
      }
    } 
  }
}

function recordToOOPRow() {
   var expense = getOOPExpenseEntries()

  var expByStaff = {};

  // Group record by staff. Get only client chargeable records
  for(var i in expense) {
    var row = expense[i]
    if((row.paymentMethod == "CASH" || row.paymentMethod == "CHEQUE") && row.adjustment == "") {
      if (typeof expByStaff[row.employeeId] !== 'undefined') {
        expByStaff[row.employeeId].push(row)
      }
      else {
        expByStaff[row.employeeId] = new Array()
        expByStaff[row.employeeId].push(row)
      }
    }
  }

  var controlSourceSheet = SpreadsheetApp.getActiveSheet()
  var range = controlSourceSheet.getDataRange()
  var values = range.getValues()
  
  var oopRow = findWord('OOP')

  for(var key in expByStaff) {
    if(expByStaff.hasOwnProperty(key)) {
      for(var i=0; i<expByStaff[key].length; i++) {
        var record = expByStaff[key][i]
        var staffPosition = findStaffPosition(controlSourceSheet, key) // Column

        var range = controlSourceSheet.getDataRange()
        if(staffPosition.found == true) {
          // (startRow, startCol, cellValue)
          rowAndColumnTrueControl(oopRow+1, staffPosition.startCol, record.amount)

        // Add new column before "Unassigned" and set amount at correct cell
        } else if (staffPosition.found == false) {
          // (startRow, startCol, columnValue, cellValue)
          rowTrueColumnFalseControl(oopRow+1, staffPosition.startCol, record.employeeId, record.amount)
        }
      }
    } 
  }
}

function recordToAdjustmentRow() {
   var expense = getOOPExpenseEntries()

  var expByStaff = {};

  // Group record by staff. Get only client chargeable records
  for(var i in expense) {
    var row = expense[i]
    if(row.adjustment == true) {
      if (typeof expByStaff[row.employeeId] !== 'undefined') {
        expByStaff[row.employeeId].push(row)
      }
      else {
        expByStaff[row.employeeId] = new Array()
        expByStaff[row.employeeId].push(row)
      }
    }
  }

  var controlSourceSheet = SpreadsheetApp.getActiveSheet()
  var range = controlSourceSheet.getDataRange()
  var values = range.getValues()
  
  var adjustmentRow = findWord('ADJUSTMENT')

  for(var key in expByStaff) {
    if(expByStaff.hasOwnProperty(key)) {
      for(var i=0; i<expByStaff[key].length; i++) {
        var record = expByStaff[key][i]
        var staffPosition = findStaffPosition(controlSourceSheet, key) // Column

        var range = controlSourceSheet.getDataRange()
        if(staffPosition.found == true) {
          // (startRow, startCol, cellValue)
          rowAndColumnTrueControl(adjustmentRow+1, staffPosition.startCol, record.amount)

        // Add new column before "Unassigned" and set amount at correct cell
        } else if (staffPosition.found == false) {
          // (startRow, startCol, columnValue, cellValue)
          rowTrueColumnFalseControl(adjustmentRow+1, staffPosition.startCol, record.employeeId, record.amount)
        }
      }
    } 
  }
  var startRow = findWord("CONTROL CHECK (GROSS ACTIVITIES)")
  var endRow = findWord("TOTAL ACTIVITIES")
  total(startRow, endRow)
}

function recordBillableHourstoControlSouce() {
  var hours = getBeeboleEntries()

  var hoursByStaff = {};

  // Group record by staff. Get only billable hours.
  for(var i in hours) {
    var row = hours[i]
    if(row.isNonWorking == false) {
      if (typeof hoursByStaff[row.employeeId] !== 'undefined') {
        hoursByStaff[row.employeeId].push(row)
      }
      else {
        hoursByStaff[row.employeeId] = new Array()
        hoursByStaff[row.employeeId].push(row)
      }
    }
  }

  var controlSourceSheet = SpreadsheetApp.getActiveSheet()
  var range = controlSourceSheet.getDataRange()
  var values = range.getValues()

  for(var key in hoursByStaff) {
    if(hoursByStaff.hasOwnProperty(key)) {
      for(var i=0; i<hoursByStaff[key].length; i++) {
        var record = hoursByStaff[key][i]
        
        var startRow = findWord('HOURS PER PROJECT')
        var endRow = findWord('TOTAL BILLABLE HOURS')
        
        var projectPosition = findProject(controlSourceSheet, startRow, endRow+1, 'HOURS PER PROJECT', record.projectId) // Row
        var staffPosition = findStaffPosition(controlSourceSheet, key) // Column

        var range = controlSourceSheet.getDataRange()
        if(staffPosition.found == true && projectPosition.found == true) {
          // (startRow, startCol, cellValue)
          rowAndColumnTrueControl(projectPosition.startRow, staffPosition.startCol, record.hours)

        // Append new row for staff and record hours to column of project
        } else if (staffPosition.found == true && projectPosition.found == false) { 
          // (startCol, rowValue, cellValue)
          rowFalseColumnTrueControl(projectPosition.startRow, staffPosition.startCol, record.projectId, record.hours)
        
        // Add new column before "Unassigned" and set hours at correct cell
        } else if (staffPosition.found == false && projectPosition.found == true) {
          // (startRow, startCol, columnValue, cellValue)
          rowTrueColumnFalseControl(projectPosition.startRow, staffPosition.startCol, record.employeeId, record.hours)

        // Append new Row for staff and insert new column before "Unassigned" and set value to project Id
        } else {
          // (startRow, startCol, columnValue, rowValue, cellValue)
          rowAndColumnFalseControl(projectPosition.startRow, staffPosition.startCol, record.employeeId, record.projectId, record.hours)
        }
      }
    } 
  }
  var startRow = findWord("HOURS PER PROJECT")
  var endRow = findWord("TOTAL BILLABLE HOURS")
  total(startRow, endRow)
}

function recordNonBillableHoursToControlSource() {

  var hours = getBeeboleEntries()

  var hoursByStaff = {};

  // Group record by staff. Get only non working records
  for(var i in hours) {
    var row = hours[i]
    if(row.isNonWorking == true) {
      if (typeof hoursByStaff[row.employeeId] !== 'undefined') {
        hoursByStaff[row.employeeId].push(row)
      }
      else {
        hoursByStaff[row.employeeId] = new Array()
        hoursByStaff[row.employeeId].push(row)
      }
    }
  }

  var controlSourceSheet = SpreadsheetApp.getActiveSheet()
  var range = controlSourceSheet.getDataRange()
  var values = range.getValues()
  
  var nonBillableRow = findWord('NON-BILLABLE')
  var holidaysRow = findWord('HOLIDAYS')

  for(var key in hoursByStaff) {
    if(hoursByStaff.hasOwnProperty(key)) {
      for(var i=0; i<hoursByStaff[key].length; i++) {
        var record = hoursByStaff[key][i]
        var staffPosition = findStaffPosition(controlSourceSheet, key) // Column
        var startRow = 0
        Logger.log("Hours project Id: " + record.projectId)
        if(record.projectId == 112) { // X00A.01 Holiday
          startRow = nonBillableRow+1
        } else { // ABSENCES HOLIDAY
          startRow = holidaysRow+1
        }

        var range = controlSourceSheet.getDataRange()
        if(staffPosition.found == true) {
          // (startRow, startCol, cellValue)
          rowAndColumnTrueControl(startRow, staffPosition.startCol, record.hours)

        // Add new column before "TOTAL" and set amount at correct cell
        } else if (staffPosition.found == false) {
          // (startRow, startCol, columnValue, cellValue)
          rowTrueColumnFalseControl(startRow, staffPosition.startCol, record.employeeId, record.hours)
        }
      }
    } 
  }
  var startRow = findWord("TOTAL BILLABLE HOURS")+1
  var endRow = findWord("TOTAL NON-BILLABLE HOURS")
  total(startRow, endRow)
}

function recordTotalHours() {

  var totalBillableHours = findWord("TOTAL BILLABLE HOURS")
  var totalNonBillableHours = findWord("TOTAL NON-BILLABLE HOURS")
  var totalHours = findWord('TOTAL HOURS')
  getSumofTwoRows(totalBillableHours+1, totalNonBillableHours+1, totalHours+1)
  
}

function clear() {

  var sheet = SpreadsheetApp.getActiveSheet()
  
  var startRow = findWord('HOURS PER PROJECT') + 2
  var endRow = findWord('TOTAL BILLABLE HOURS') + 1

  var numRows = endRow - startRow
  
  sheet.deleteRows(startRow, numRows)
  
  startRow = findWord('PROJECTS') + 2
  endRow = findWord('TOTAL BILLABLE EXPENSES') + 1
  
  numRows = endRow - startRow
  
  sheet.deleteRows(startRow, numRows)
  
  var totalPosition = findStaffPosition(sheet, "TOTAL")
  totalPosition = totalPosition.startCol
  
  var numColumns = totalPosition - 5
  
  sheet.deleteColumns(5, numColumns)
  
  var range = sheet.getDataRange()
  var values = range.getValues()
  
  for(var i = 10; i<=values.length; i ++) {
    var cell = range.getCell(i, range.getLastColumn()).setValue("")
  }

}